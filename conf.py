# -*- encoding: utf-8 -*-
# This is your configuration file.  Please write valid python!
# See http://posativ.org/acrylamid/conf.py.html

SITENAME = 'A descriptive blog title'
WWW_ROOT = '/en/getstarted' # folder where project is published on webserver
WWW_ASSETS = '/en/latest' # folder that contains css and javascript
IMAGES = '/en/getstarted/images'
VIDEO = '/en/getstarted/video'
ENCODING = 'utf-8'

AUTHOR = 'SaltStack, Inc.'
EMAIL = ''

FILTERS = ['md', 'jinja2', 'sidebar']
VIEWS = {
    # # '/:slug/' is a slugified url of your static page's title
    '/:slug.html': {'view': 'page'},
}
OUTPUT_DIR = '_build/html'
THEME = 'theme'
ENGINE = 'acrylamid.templates.jinja2.Environment'
DATE_FORMAT = '%d.%m.%Y, %H:%M'
CONTENT_IGNORE = ['.git*', '.hg*', '.svn']
CONTENT_EXTENSION = '.md'
STATIC = ['content/_static']
STATIC_IGNORE = ['.git*', '.hg*', '.svn']
DEPLOYMENT = {
}
