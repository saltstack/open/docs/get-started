---
title: Communication
permalink: system/communication.html
type: page
layout: getstarted.tmpl
series: Understanding SaltStack
step: 3
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
