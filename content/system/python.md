---
title: Python
permalink: system/python.html
type: page
layout: getstarted.tmpl
series: Understanding SaltStack
step: 8
next:
  title: Continue to SaltStack Fundamentals
  url: fundamentals/
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
