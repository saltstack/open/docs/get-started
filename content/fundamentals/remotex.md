---
title: Execute Commands
permalink: fundamentals/remotex.html
type: page
layout: getstarted.tmpl
series: SaltStack Fundamentals
step: 3
overview:
  goals:
    - Remotely execute shell commands on managed systems
    - Remotely execute Salt execution modules on managed systems
  time: 5
  difficulty: 1
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
