---
title: Install SaltStack
permalink: fundamentals/install.html
type: page
layout: getstarted.tmpl
series: SaltStack Fundamentals
step: 2
overview:
  goals:
    - Install SaltStack
    - View and accept connections from managed systems
    - Send a test command to all managed systems
  time: 10
  difficulty: 1
modals:
  - id: install-modal
    title: Install SaltStack
    message: |
      #### On the Salt Master


    button: Got It!
  - id: install-video-modal
    title: SaltStack Installation Demo
    className: modal90
    message: <video>supports HTML5 video</a></p> </video>
    button: Close
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
