---
title: Apply and Target States
permalink: fundamentals/top.html
type: page
layout: getstarted.tmpl
series: SaltStack Fundamentals
step: 6
overview:
  goals:
    - Create a Top file to match systems with Salt state files
  time: 15
  difficulty: 2
next:
  title: Continue to Configuration Management Get Started
  url: config/
modals:
  - id: highstate-modal
    title: Highstate?
    message: |
      A highstate causes all targeted minions to download the
      `/srv/salt/top.sls` file and find any matching targets. If a matching
      target is found, the minion applies all of the states listed under that
      target. Many users schedule highstate runs at regular intervals to ensure
      that systems remain in compliance.
    button: Got It!
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
