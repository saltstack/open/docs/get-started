---
title: Create a Salt State
permalink: fundamentals/states.html
type: page
layout: getstarted.tmpl
series: SaltStack Fundamentals
step: 5
overview:
  goals:
    - Define a reusable configuration definition.
  time: 15
  difficulty: 2
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
