---
title: Salt Cloud Provider
permalink: cloud/provider.html
type: page
layout: getstarted.tmpl
series: Salt Cloud
step: 2
overview:
  goals:
    - Goal
    - Goal
  time: 10
  difficulty: 1
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
