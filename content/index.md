---
title: Introduction
type: page
layout: getstarted-intro.tmpl
permalink: index.html
imgmap: True
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
