---
title: Custom Events
permalink: event/custom.html
type: page
layout: getstarted.tmpl
series: Event-Driven Infrastructure
step: 3
overview:
  goals:
    - Enable additional Salt events
    - Send custom events
  time: 10
  difficulty: 1
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
