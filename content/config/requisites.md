---
title: Ordering & Requisites
permalink: config/requisites.html
type: page
layout: getstarted.tmpl
series: SaltStack Configuration Management
step: 5
overview:
  goals:
    - Understand the default execution order
    - Use requisites to declare execution order
  time: 15
  difficulty: 2
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
