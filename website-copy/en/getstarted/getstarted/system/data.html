<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>System Data</title>

    <link rel="stylesheet" href="/en/latestcss/core.css">
    <link rel="stylesheet" href="/en/latestcss/getstarted.css">
    <link rel="shortcut icon" href="/en/latestcss/images/saltStack_favIcon_32x32.png">

</head>
<body>

<nav id="globalNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="http://saltstack.com/" target="_blank"><img src="/en/latestcss/images/saltstack.svg" height="40px" width="170px" class="nolightbox"></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="nav navbar-nav">
                <li><a href="/en/latest">Documentation</a></li>
                <li><a href="/en/getstarted/">Tutorials</a></li>
                <li><a href="/en/latestcontents.html">Reference Guide</a></li>
                <li><a href="https://repo.saltstack.com">Downloads</a></li>
                <li><a href="/en/latesttopics/development/">Develop</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="jumbotron">

    <div class="row row-centered">
        <div class="col-md-9 col-centered">
            <h1>Understanding SaltStack</h1>

            <h3>Get Started Tutorial</h3>

        </div>
    </div>
    <!--<div class="gs-top-nav">
        <a href="/en/getstarted/getstarted/index.html">Back to Get Started <i class="glyphicon glyphicon-arrow-left"></i></a><br>
        <a href="/en/getstarted">Back to Documentation <i class="glyphicon glyphicon-arrow-left"></i></a>
    </div>-->
</div>







<div class="container">
    <div class="row">
        <div class="col-md-12">
            <ol class="progtrckr" data-progtrckr-steps="8">
                
                
                
                <li class="progtrckr-done">
                    
                    <a href="/en/getstarted/system/index.html">Salt Approach</a>
                </li>
                
                
                
                
                <li class="progtrckr-done">
                    
                    <a href="/en/getstarted/system/plugins.html">Plug-ins</a>
                </li>
                
                
                
                
                <li class="progtrckr-done">
                    
                    <a href="/en/getstarted/system/communication.html">Communication</a>
                </li>
                
                
                
                
                <li class="progtrckr-done">
                    
                    <a href="/en/getstarted/system/execution.html">Remote Execution</a>
                </li>
                
                
                
                
                <li class="progtrckr-done">
                    
                    <a href="/en/getstarted/system/states.html">States</a>
                </li>
                
                
                
                
                <li class="progtrckr-done">
                    
                    <a href="/en/getstarted/system/runners.html">Runners</a>
                </li>
                
                
                
                <li class="progtrckr-done">System Data</li>
                
                
                
                
                
                
                
                
                
                
                
                
                
                <li class="progtrckr-todo">
                    
                    <a href="/en/getstarted/system/python.html">Python</a>
                </li>
                
                
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">

            

            <h1 id="data">Data</h1>
<p>This section discusses the two primary aspects of system data: getting data <em>about</em> your systems, and getting data <em>to</em> your systems. Salt provides two different subsystems to perform each of these tasks respectively: Salt grains and Salt pillar.</p>
<p>This section also explains a third component, called Salt mine, which is used to push data from a minion to a shared data store on the master that can be accessed by all minions.</p>
<h2 id="grains">Grains</h2>
<div class="section sidebar">
<h4 id="how-are-grains-gathered">How are grains gathered?</h4>
<p>If you’ve read the earlier section on plug-ins, you already know how grains work. The grains subsystem has several Salt grains modules (aka plug-ins) that run on the minion to populate the grains dictionary.</p>
<p>Since grains are simpler than remote execution modules, each grain module contains the logic to gather grain data across OSs (instead of using multiple modules and <code>__virtualname__</code>).</p>
</div>
<p>Grains are used to get data about your systems. Grains are static information about the underlying operating system, memory, disks, and many other system properties.</p>
<p>Grains are gathered automatically when the minion starts and are refreshed periodically or by using a remote execution command.</p>
<p>So what can you do with grains? You can gather inventory using the grains execution module, which lets you list all grains, find systems with a specific grain value, and so on.</p>
<p>Grains are also an integral part of the targeting system. Grains are used to target salt states and Salt pillar data, which leads us directly to the next minion data subsystem: Salt pillar.</p>
<h2 id="salt-pillar">Salt Pillar</h2>
<p>Salt pillar is used to deliver data to your systems. Think about the different custom data needed when configuring even a simple system: user names, service URLs, preferred installation paths, ports, non-default application settings, and many others. Often these values are different for each system or system role (web, database, and so on).</p>
<p>Salt pillar lets you define these data values and then assign them to one or more minions using targets. The values can then be inserted into Salt states using variables.</p>
<p>Salt pillar data is encrypted using the targeted minion’s public key and sent over a secure channel, so Salt pillar is also well-suited to distribute secure data such as passwords and ssh keys since it can be decrypted only by the targeted minion. Salt pillar data is never written to disk on the minion.</p>
<p>The default Salt pillar module defines pillar using a YAML file, though over 30 Salt pillar modules (aka plug-ins) are available to support a wide-variety of backends. Popular options include Mongo and Redis, which are both designed to store structured data. Many users stick with YAML files, but use a private git repo to manage and distribute the pillar data.</p>
<h2 id="salt-mine">Salt Mine</h2>
<p>The Salt mine is used to share data values among Salt minions. For example, if you set up a shared database, you can configure the Salt minion that is running on the database server to push its IP address to the Salt mine automatically. This is a better approach than storing it in a Salt state or in Salt pillar where it needs to be manually updated.</p>
<p>Later when you are setting up a system that requires this value, you can access the Salt mine directly from a Salt state file.</p>
<p>The Salt mine is very easy to use, the biggest issue for many users is remembering to use it!</p>
<p>The rest of the essential subsystems are covered in other Get Started guides, so we’ll wrap up by covering a few Python basics that will help you use Salt more effectively.</p>


            <br>
            
            <a href="/en/getstarted/system/runners.html">
                <button type="button" class="btn btn-secondary"><span
                        class="glyphicon glyphicon-chevron-left"></span> Previous
                </button>
            </a>
            

            
            <a href="/en/getstarted/system/python.html">
                <button type="button" class="btn btn-primary btn-right">
                    Next <span class="glyphicon glyphicon-chevron-right"></span></button>
            </a>
            

        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="footer">
    <p>© 2020 SaltStack | <a href="http://saltstack.com/privacy-policy" target="_blank">Privacy Policy</a></p>
</div>
</div>

<script type="text/javascript" src="/en/latestjs/core-min.js"></script>



<!--analytics-->
<script type="text/javascript" language="javascript">llactid=23943</script>
<script type="text/javascript" language="javascript" src="https://trackalyzer.com/trackalyze_secure.js"></script>

<script>
          var _gaq = _gaq || [];
          var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
          _gaq.push(['_require', 'inpage_linkid', pluginUrl]);
          _gaq.push(['_setAccount', 'UA-26984928-1']);
          _gaq.push(['_setDomainName', 'saltstack.com']);
          _gaq.push(['_trackPageview']);

          (function() {
              var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
              ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();

</script>

</body>
</html>