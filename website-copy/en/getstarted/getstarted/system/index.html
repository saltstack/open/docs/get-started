<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Salt Approach</title>

    <link rel="stylesheet" href="/en/latestcss/core.css">
    <link rel="stylesheet" href="/en/latestcss/getstarted.css">
    <link rel="shortcut icon" href="/en/latestcss/images/saltStack_favIcon_32x32.png">

</head>
<body>

<nav id="globalNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="http://saltstack.com/" target="_blank"><img src="/en/latestcss/images/saltstack.svg" height="40px" width="170px" class="nolightbox"></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="nav navbar-nav">
                <li><a href="/en/latest">Documentation</a></li>
                <li><a href="/en/getstarted/">Tutorials</a></li>
                <li><a href="/en/latestcontents.html">Reference Guide</a></li>
                <li><a href="https://repo.saltstack.com">Downloads</a></li>
                <li><a href="/en/latesttopics/development/">Develop</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="jumbotron">

    <div class="row row-centered">
        <div class="col-md-9 col-centered">
            <h1>Understanding SaltStack</h1>

            <h3>Get Started Tutorial</h3>

        </div>
    </div>
    <!--<div class="gs-top-nav">
        <a href="/en/getstarted/getstarted/index.html">Back to Get Started <i class="glyphicon glyphicon-arrow-left"></i></a><br>
        <a href="/en/getstarted">Back to Documentation <i class="glyphicon glyphicon-arrow-left"></i></a>
    </div>-->
</div>







<div class="container">
    <div class="row">
        <div class="col-md-12">
            <ol class="progtrckr" data-progtrckr-steps="8">
                
                
                <li class="progtrckr-done">Salt Approach</li>
                
                
                
                
                
                
                
                
                
                
                <li class="progtrckr-todo">
                    
                    <a href="/en/getstarted/system/plugins.html">Plug-ins</a>
                </li>
                
                
                
                
                <li class="progtrckr-todo">
                    
                    <a href="/en/getstarted/system/communication.html">Communication</a>
                </li>
                
                
                
                
                <li class="progtrckr-todo">
                    
                    <a href="/en/getstarted/system/execution.html">Remote Execution</a>
                </li>
                
                
                
                
                <li class="progtrckr-todo">
                    
                    <a href="/en/getstarted/system/states.html">States</a>
                </li>
                
                
                
                
                <li class="progtrckr-todo">
                    
                    <a href="/en/getstarted/system/runners.html">Runners</a>
                </li>
                
                
                
                
                <li class="progtrckr-todo">
                    
                    <a href="/en/getstarted/system/data.html">System Data</a>
                </li>
                
                
                
                
                <li class="progtrckr-todo">
                    
                    <a href="/en/getstarted/system/python.html">Python</a>
                </li>
                
                
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">

            

            <p>You can get a general understanding of how Salt works by seeing it in action. That said, there are many things going on under the hood that can be hard to notice as data files past on the console. This section of the Get Started tutorial explains how Salt works, the Salt subsystems, and how Salt’s modular architecture let’s you use and extend Salt to manage your entire infrastructure.</p>
<p>Before we get into the specifics of the Salt components, it is helpful to understand a few things about the way Salt approaches infrastructure management.</p>
<h2 id="real-time-communication">Real-time communication</h2>
<p>All Salt minions receive commands simultaneously. This means that the time it takes to update 10 or 10,000 systems is quite similar, and queries to thousands of systems can be done in seconds. The Salt way to get information about your infrastructure is to query it in real time rather than rely on an (often outdated) database.</p>
<p>(Note: Databases are great for storing job results, and Salt supports over 30 job results plug-ins including mysql, elasticsearch, syslog, carbon, and reddis. Salt also supports interfacing with many data stores to provide secure configuration data. When it comes to decisions that depend on the current system configuration though, Salt views a static database a bit like asking each person in the room a question by sending a separate text message and then writing the answer in a notebook.)</p>
<h2 id="no-freeloaders">No freeloaders!</h2>
<p>Salt minions do their own work. Communication from the Salt master is a lightweight set of instructions that basically says “if you are a minion with these properties: run this command with these arguments.” Salt minions determine if they match the properties when the command is received. Each Salt minion already has all of the commands that it needs stored locally, so the command can be executed and the results quickly returned back to the Salt master. The Salt master doesn’t do anything for a minion that it can do (often better) on its own.</p>
<h2 id="salt-loves-to-scale">Salt loves to scale</h2>
<p>Salt is designed for high-performance and scalability. Salt’s communication system establishes a persistent data pipe between the Salt master and minions using ZeroMQ or raw TCP, giving Salt considerable performance advantages over competing solutions. Messages are efficiently serialized on the wire using MessagePack. Internally, Salt uses Python Tornado (implemented by some really smart developers) as an asynchronous networking library, and Salt is tuned using leading-edge approaches to multi-threading and concurrency.</p>
<p>It is not uncommon to meet users with over 10,000 minions on a single master in production, and there are known deployments with over 35,000 minions on a single Salt master! Salt has proven real-world speed and scalability.</p>
<h2 id="normalize-everything">Normalize everything</h2>
<p>Normalization is the key to Salt’s cross-platform management capabilities. Salt commands and states run the same whether you are targeting Linux, Windows, MacOS, FreeBSD, Solaris, or AIX, are on physical hardware or in the cloud, or if you are targeting a container. Salt abstracts the details of each OS, hardware type, and system tools so you can get right to managing your infrastructure.</p>
<p>Everything also includes returns: Salt commands return results in a consistent data structure for easy consumption and storage.</p>
<h2 id="manage-everything">Manage everything</h2>
<p>Salt runs nearly everywhere that Python runs. For devices that can’t quite manage Python, you can use the proxy minion system. This means that the only requirement to be managed by Salt is to support <em>any</em> network protocol (even if you wrote the protocol yourself!). Salt commands are sent to the proxy minion, which translates the salt calls into the native protocol, and then sends them to the device. The return data from the device is parsed, and then placed into a data structure and returned.</p>
<h2 id="automate-everything">Automate everything</h2>
<p>Salt’s event-driven infrastructure not only lets you automate initial system configuration, it lets you automate as-you-go to scale, repair, and perform ongoing management. Salt users automate deployment and maintenance of complex distributed network applications and databases, custom applications, files, user accounts, standard packages, cloud resources, and much more.</p>
<h2 id="programming-optional">Programming Optional</h2>
<p>You can use all of the salt capabilities without learning a programming language. Salt’s remote execution capabilities are CLI commands, and the State system uses YAML to describe the desired configuration of a system.</p>
<p>You can still take the “infrastructure as code” approach—Salt has many tools to support this including a powerful requisite system with imperative and declarative execution. Salt just saves you from needing to write Python or Ruby code to describe your infrastructure.</p>
<h2 id="extensibility-through-modular-systems">Extensibility through Modular Systems</h2>
<p>Some management tools consider themselves extensible because they can run an external script. In Salt, <em>everything</em> is extensible. As you’ll learn in the next section, even the underlying communication protocol is interchangeable. Salt has over 20 extensible subsystems, which means that if Salt can do it, then you can customize how it is done.</p>
<p>Salt can quickly adopt new technologies and manage new applications as they are developed, and you’ll never be stuck with someone else’s decision about which is the best way to manage your infrastructure.</p>
<h2 id="plug-ins">Plug-ins</h2>
<p>In the next section, we’ll learn about Salt plug-ins and why they are so critical to the Salt approach.</p>


            <br>
            

            
            <a href="/en/getstarted/system/plugins.html">
                <button type="button" class="btn btn-primary btn-right">
                    Next <span class="glyphicon glyphicon-chevron-right"></span></button>
            </a>
            

        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="footer">
    <p>© 2020 SaltStack | <a href="http://saltstack.com/privacy-policy" target="_blank">Privacy Policy</a></p>
</div>
</div>

<script type="text/javascript" src="/en/latestjs/core-min.js"></script>



<!--analytics-->
<script type="text/javascript" language="javascript">llactid=23943</script>
<script type="text/javascript" language="javascript" src="https://trackalyzer.com/trackalyze_secure.js"></script>

<script>
          var _gaq = _gaq || [];
          var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
          _gaq.push(['_require', 'inpage_linkid', pluginUrl]);
          _gaq.push(['_setAccount', 'UA-26984928-1']);
          _gaq.push(['_setDomainName', 'saltstack.com']);
          _gaq.push(['_trackPageview']);

          (function() {
              var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
              ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();

</script>

</body>
</html>