<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Remote Execution</title>

    <link rel="stylesheet" href="/en/latest/css/core.css">
    <link rel="stylesheet" href="/en/latest/css/getstarted.css">
    <link rel="shortcut icon" href="/en/latest/css/images/saltStack_favIcon_32x32.png">

</head>
<body>

<nav id="globalNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="http://saltstack.com/" target="_blank"><img src="/en/latest/css/images/saltstack.svg" height="40px" width="170px" class="nolightbox"></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="nav navbar-nav">
                <li><a href="/en/latest/">Overview</a></li>
                <li><a href="/en/getstarted/">Tutorials</a></li>
                <li><a href="/en/latest/contents.html">Documentation</a></li>
                <li><a href="https://repo.saltstack.com">Downloads</a></li>
                <li><a href="/en/latest/topics/development/">Develop</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="jumbotron">

    <div class="row row-centered">
        <div class="col-md-9 col-centered">
            <h1>Understanding SaltStack</h1>

            <h3>Get Started Tutorial</h3>

        </div>
    </div>
    <!--<div class="gs-top-nav">
        <a href="/en/getstarted/getstarted/index.html">Back to Get Started <i class="glyphicon glyphicon-arrow-left"></i></a><br>
        <a href="/en/getstarted">Back to Documentation <i class="glyphicon glyphicon-arrow-left"></i></a>
    </div>-->
</div>







<div class="container">
    <div class="row">
        <div class="col-md-12">
            <ol class="progtrckr" data-progtrckr-steps="8">
                
                
                
                <li class="progtrckr-done">
                    
                    <a href="/en/getstarted/system/index.html">Salt Approach</a>
                </li>
                
                
                
                
                <li class="progtrckr-done">
                    
                    <a href="/en/getstarted/system/plugins.html">Plug-ins</a>
                </li>
                
                
                
                
                <li class="progtrckr-done">
                    
                    <a href="/en/getstarted/system/communication.html">Communication</a>
                </li>
                
                
                
                <li class="progtrckr-done">Remote Execution</li>
                
                
                
                
                
                
                
                
                
                
                
                
                
                <li class="progtrckr-todo">
                    
                    <a href="/en/getstarted/system/states.html">States</a>
                </li>
                
                
                
                
                <li class="progtrckr-todo">
                    
                    <a href="/en/getstarted/system/runners.html">Runners</a>
                </li>
                
                
                
                
                <li class="progtrckr-todo">
                    
                    <a href="/en/getstarted/system/data.html">System Data</a>
                </li>
                
                
                
                
                <li class="progtrckr-todo">
                    
                    <a href="/en/getstarted/system/python.html">Python</a>
                </li>
                
                
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">

            

            <h1 id="remote-execution">Remote Execution</h1>
<p><em>This tutorial describes how the remote execution system works. For a tutorial on using remote execution, see <a href="/en/getstarted/fundamentals/remotex.html">Execute Commands</a>.</em></p>
<p>Salt was designed from the beginning to be a remote execution tool, and remote execution is used heavily by the other subsystems in Salt.</p>
<ul>
<li><p>Salt commands are designed to work cross-OS and cross-platform. A <code>salt '*' pkg.install git</code> command uses yum, apt, pacman, or Salt’s windows package repository under the covers based on the target platform. One command, many systems.</p></li>
<li><p>All Salt commands return results in a consistent data structure. This makes it easy to examine results or store them in a database.</p></li>
<li><p>All targeted systems can run a job at the same time since they all receive the command simultaneously.</p></li>
<li><p>Salt uses a library of hundreds of Python modules to perform remote management, and you can easily add your own (or better yet, contribute them to the project!). Any application or service that can be accessed using Python, a shell command, or nearly any other interface can be exposed as an execution module in Salt.</p></li>
</ul>
<h2 id="remote-execution-1">Remote Execution</h2>
<p>The remote execution system is accessed using the <code>salt</code> command-line interface. Let’s take a simple command sent from the Salt interface, and trace it through the Salt system:</p>
<pre class="language-bash"><code class="language-bash"><span class="kw">salt</span> <span class="st">&#39;*&#39;</span> test.rand_sleep 120</code></pre>
<p>Based on what we learned about the Salt communication system, here is what happens:</p>
<ol style="list-style-type: decimal">
<li>This command is sent over the publisher port to all connected minions.</li>
<li>Each minion examines the command, evaluates itself against the target, and decides whether or not it should run the command.</li>
<li>Targeted systems run the command and then return the results to the request server.</li>
</ol>
<p>Let’s dig in and take a closer look at what happens on the minion when the command runs. First, each command is spun off into a separate worker thread, so a Salt minion can handle multiple jobs at once.</p>
<p>But how does the Salt minion turn the <code>test.rand_sleep 120</code> command into action? Salt execution modules of course!</p>
<p>All of the commands that are used to manage systems are provided by Salt execution modules. These modules are what do the actual work in Salt, and are easy to use once you know a few things about how they work.</p>
<div class="section sidebar">
<h4 id="code-dive">Code Dive!</h4>
<p>If you enjoy a code dive (who doesn’t!) take a look at the <a href="https://github.com/saltstack/salt/blob/develop/salt/modules/test.py#L139"><code>rand_sleep</code></a> function and see if you can find the relationship between the arguments sent on the command line and the arguments sent to the function (spoiler alert: they are exactly the same, even down to the name).</p>
</div>
<p>When the Salt minion receives a command, it simply finds the correct module (in this example, the <code>test</code> module), and then calls the corresponding function (<code>rand_sleep</code>) providing the supplied arguments (<code>120</code>). In this sense, you can think of Salt as a (crazy powerful) abstraction layer to Python functions.</p>
<p><img width="50%" class="imgcenter" src="/en/getstarted/images/cmd-syntax2.png"></p>
<p>When using remote execution, you’ll want to bookmark the <a href="https://docs.saltstack.com/en/latest/ref/modules/all/index.html">complete list of execution modules</a>. If you are ever wondering if Salt can manage a Tomcat server, this is your first stop (note: yes, it can). The module docs have many examples, and are simple to call.</p>
<p>Modules and functions are discussed again in the Python section, so let’s move on for now and discuss the configuration management subsystem, Salt states.</p>


            <br>
            
            <a href="/en/getstarted/system/communication.html">
                <button type="button" class="btn btn-secondary"><span
                        class="glyphicon glyphicon-chevron-left"></span> Previous
                </button>
            </a>
            

            
            <a href="/en/getstarted/system/states.html">
                <button type="button" class="btn btn-primary btn-right">
                    Next <span class="glyphicon glyphicon-chevron-right"></span></button>
            </a>
            

        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="footer">
  <p>© 2020 SaltStack. All Rights Reserved, SaltStack Inc. | <a href="http://saltstack.com/privacy-policy" target="_blank">Privacy Policy</a></p>
</div>
</div>

<script type="text/javascript" src="/en/latest/js/core-min.js"></script>



<!--analytics-->
<script type="text/javascript">llactid=23943</script>
<script type="text/javascript" src="https://trackalyzer.com/trackalyze_secure.js"></script>

<script>
          var _gaq = _gaq || [];
          var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
          _gaq.push(['_require', 'inpage_linkid', pluginUrl]);
          _gaq.push(['_setAccount', 'UA-26984928-1']);
          _gaq.push(['_setDomainName', 'saltstack.com']);
          _gaq.push(['_trackPageview']);

          (function() {
              var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
              ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();

</script>

</body>
</html>